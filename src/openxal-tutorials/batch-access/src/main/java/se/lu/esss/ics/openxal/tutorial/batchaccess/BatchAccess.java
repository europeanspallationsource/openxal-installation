package se.lu.esss.ics.openxal.tutorial.batchaccess;

import java.util.ArrayList;
import java.util.List;

import xal.ca.BatchGetValueRequest;
import xal.ca.Channel;
import xal.ca.ChannelRecord;
import xal.sim.scenario.Scenario;
import xal.smf.AcceleratorNode;
import xal.smf.AcceleratorSeq;
import xal.smf.data.XMLDataManager;
import xal.smf.impl.BPM;
import xal.smf.impl.Electromagnet;
import xal.smf.proxy.PrimaryPropertyAccessor;

public class BatchAccess {
	public static void main(String[] args) {
		// loads accelerator
		AcceleratorSeq acc = XMLDataManager.acceleratorWithPath(XMLDataManager.defaultPath());
		
		// selects all quads
		List<AcceleratorNode> quads = acc.getAllNodesOfType("Q");
		// selects all BPMs
		List<AcceleratorNode> bpms = acc.getAllNodesOfType("BPM");
		
		
	
		///// Following code calls batch caget on all channels, but works only for magnets, gaps & cavities (not bpms)
		// It could be extended to work for BPMs too
		//
		PrimaryPropertyAccessor propertyAccessor = new PrimaryPropertyAccessor();
		propertyAccessor.requestValuesForNodes(quads, Scenario.SYNC_MODE_LIVE );
		for (AcceleratorNode quad : quads) {
			System.out.printf("%s %E\n", quad.getId(), propertyAccessor.valueMapFor(quad).get(Electromagnet.Property.FIELD.name()));	
		}
		
		/////////////////////////
		
		
		
		//// Second option works with BPMs, but requires a bit more work and is more low level
		List<Channel> channels = new ArrayList<>();
		for (AcceleratorNode bpm : bpms) channels.add(bpm.getChannel(BPM.X_AVG_HANDLE));
		final BatchGetValueRequest request = new BatchGetValueRequest( channels );
		request.submitAndWait( 5.0 );	// wait up to 5 seconds for a response

		// print an overview of the request status
		if ( !request.isComplete() ) {
			final int requestCount = channels.size();
			final int recordCount = request.getRecordCount();
			final int exceptionCount = request.getExceptionCount();
			System.err.println( "Batch channel request for online model is incomplete. " + recordCount + " of " + requestCount + " channels succeeded. " + exceptionCount + " channels had exceptions." );
		}

		for ( final Channel channel : channels ) {
			final ChannelRecord record = request.getRecord( channel );
			if ( record != null ) {
				System.out.printf("%s: %E\n", channel.channelName(), record.doubleValue());
			} else
			{
				System.out.printf("%s: N/A\n", channel.channelName());
			}
		}
		//////////////////7
	}
}
