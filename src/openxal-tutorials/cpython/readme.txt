-== Instruction about running the CPython demo ==-

What is needed:
    - CPython >= 2.6
    - Installed Py4J version 0.8.1
    
    
= Installing Setup Tools =

    To install Py4J easiest way is to use Python setup tools.

    Please do 'sudo yum install python-setuptools' in your Scientific Linux (or any Fedora based system).
 
= Installing Py4J =
 
    To install, one could use 'sudo easy_install py4j'' in the Terminal.
    Please note this will fail if Python setuptools (see above) are not 
installed for the Python distribution.
        
        
= Running the example =

To run the exmaple a Java server must be started. To do this please use the Terminal 
and execute the following commands (replace PROJECTS_DIR with your own location of
openxal-tutorials project):
    
    cd PROJECTS_DIR/openxal-tutorials/cpython/py4j-server
    mvn package 
    ./run
    

With the Java server running, please open another Terminal and invoke the script:
    
    cd PROJECTS_DIR/openxal-tutorials/cpython
    python demo_cpython.py
    
