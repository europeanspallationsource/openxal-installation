package se.lu.esss.ics.openxal.tutorial.xalplot.step1;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;

import xal.extension.application.Commander;
import xal.extension.application.XalWindow;
import xal.extension.widgets.plot.FunctionGraphsJPanel;

/**
 * It defines a window for the document.
 * The document window class must define the main window used to present the document to the user.
 * The developer is free to lay out the window as desired
 */
public class Window extends XalWindow {
	private static final long serialVersionUID = 1L;

	protected JTabbedPane tabbedPane;
	protected FunctionGraphsJPanel plot;
	protected JTable dataTable;
	
	/**
	 * Creates a window presenting the document
	 * @param document The document to load
	 */
	public Window(Document document) {
		super(document);
		setSize(600, 600);
		makeContent(document);
	}	

	/**
	 * Lays out the window.
	 */
	protected void makeContent(Document document) {
		tabbedPane = new JTabbedPane();

		JTable dataTable = new JTable();
		JPanel dataTablePanel = new JPanel(new BorderLayout());
		dataTablePanel.add(new JScrollPane(dataTable));
		tabbedPane.addTab("Data", dataTablePanel);

		plot = new FunctionGraphsJPanel();
		tabbedPane.addTab("Plot", plot);

		getContentPane().add(tabbedPane);

	}
	
	/**
	 * Adds actions to the menus
	 */
	@Override
	protected void customizeCommands(Commander commander) {		
		// TODO add menu actions
	}
}
