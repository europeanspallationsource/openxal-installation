package se.lu.esss.ics.openxal.tutorial.acceleratorapp.finalapp;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.table.AbstractTableModel;

import xal.extension.application.Commander;
import xal.extension.application.FrameApplication;
import xal.model.ModelException;
import xal.model.alg.EnvelopeTracker;
import xal.model.alg.Tracker;
import xal.model.probe.EnvelopeProbe;
import xal.model.probe.Probe;
import xal.model.probe.traj.EnvelopeProbeState;
import xal.model.probe.traj.ProbeState;
import xal.model.probe.traj.Trajectory;
import xal.model.xml.ParsingException;
import xal.model.xml.ProbeXmlParser;
import xal.sim.scenario.ModelInput;
import xal.sim.scenario.Scenario;
import xal.smf.Accelerator;
import xal.smf.AcceleratorNode;
import xal.smf.AcceleratorSeq;
import xal.extension.application.smf.AcceleratorDocument;
import xal.tools.beam.Twiss;
import xal.tools.data.DataAdaptor;
import xal.tools.data.DataListener;
import xal.extension.widgets.plot.BasicGraphData;
import xal.tools.xml.XmlDataAdaptor;

/**
 * Document class is a holder for document contents. It provides the framework
 * with document specific information and also provides hooks for document
 * control. The application framework allows the developer the freedom to encode
 * their document as they wish.
 */
public class Document extends AcceleratorDocument {
	/** Probe used for simulation */
	private Probe probe;
		
	/** Scenario for simulation */
	private Scenario scenario;
	
	/** Modifiable parameters */
	private List<ModelInput> modifiableParameters = new ArrayList<ModelInput>();
	
	/** Graph data */
	private BasicGraphData twissX = new BasicGraphData();
	private BasicGraphData twissY = new BasicGraphData();
	private BasicGraphData twissZ = new BasicGraphData();
	private BasicGraphData energy = new BasicGraphData();
	

	private ParametersTableModel parametersTableModel = new ParametersTableModel();
	
	/** Probe file chooser */
	private JFileChooser probeFileChooser = new JFileChooser(); 

	

	/**
	 * Empty constructor to create empty document
	 */
	public Document() {
		twissX.setGraphName("beta_x");
		twissY.setGraphName("beta_y");
		twissZ.setGraphName("beta_z");
		energy.setGraphName("energy");
		twissX.setGraphProperty("Legend", "beta_x");
		twissY.setGraphProperty("Legend", "beta_y");
		twissZ.setGraphProperty("Legend", "beta_z");
		energy.setGraphProperty("Legend", "energy");
		twissX.setGraphColor(Color.RED);
		twissY.setGraphColor(Color.GREEN);
		twissZ.setGraphColor(Color.BLUE);
		
		setupProbe();
	}

	/**
	 * This method is called when the window needs to be shown. Although dirty,
	 * variable mainWindow has to be set to point to the window representing
	 * this document.
	 * 
	 * @see xal.application.XalAbstractDocument#makeMainWindow()
	 */
	@Override
	protected void makeMainWindow() {
		mainWindow = new Window(this);
	}

	/**
	 * We hardcoded an initial probe that is used. This decision is made for
	 * lazy user so they won't complain when trying out the application.
	 * */
	private void setupProbe() {
		// Envelope probe and tracker
		EnvelopeTracker envelopeTracker = new EnvelopeTracker();
		envelopeTracker.setRfGapPhaseCalculation(false);
		envelopeTracker.setUseSpacecharge(true);
		envelopeTracker.setEmittanceGrowth(false);
		envelopeTracker.setStepSize(0.004);
		envelopeTracker.setProbeUpdatePolicy(Tracker.UPDATE_EXIT);

		EnvelopeProbe envelopeProbe = new EnvelopeProbe();
		envelopeProbe.setAlgorithm(envelopeTracker);
		envelopeProbe.setSpeciesCharge(-1);
		envelopeProbe.setSpeciesRestEnergy(9.39294e8);
		envelopeProbe.setKineticEnergy(2500000);
		envelopeProbe.setPosition(0.0);
		envelopeProbe.setTime(0.0);
		envelopeProbe.initFromTwiss(new Twiss[] {
				new Twiss(-1.62, 0.155, 3.02e-6),
				new Twiss(3.23, 0.381, 3.46e-6),
				new Twiss(0.0196, 0.5844, 3.8638e-6) });
		envelopeProbe.setBeamCurrent(0.02);
		envelopeProbe.setBunchFrequency(4.025e8);

		probe = envelopeProbe;
	}

	/**
	 * This procedure is overriden from AcceleratorApplication and is called
	 * when user selects or changes the sequence. It takes care of regenerating
	 * the scenario and loading modifiableParameters.
	 * */
	@Override
	public void selectedSequenceChanged() {
		AcceleratorSeq seq = getSelectedSequence();

		scenario = null;
		modifiableParameters.clear();

		if (seq != null) {
			try {
				// generates the scenario
				scenario = Scenario.newScenarioFor(seq);
				if (scenario != null) scenario.setProbe(probe);

				// loads modifiable parameters
				for (AcceleratorNode node : seq.getNodes()) {
					try {
						Map<String, Double> props = scenario
								.propertiesForNode(node);
						if (props != null && !props.isEmpty()) {
							for (Map.Entry<String, Double> mapEntry : props
									.entrySet()) {
								modifiableParameters
										.add(new ModelInput(node, mapEntry
												.getKey(), mapEntry.getValue()));
							}
						}
					} catch (IllegalArgumentException e) {
						// this exception happens on nodes without parameters
						// we catch it, but don't display it
					}
				}
			} catch (ModelException e) {
				FrameApplication.displayError("Generating scenario error",
								"Error while generating scenario & collecting parameters", e);
				scenario = null;
			}
		}

		// updates the gui
		parametersTableModel.fireTableDataChanged();

		// updates the section in the window
		// null check because it might happen before we have a window (i.e. opening document at start)
		if (mainWindow != null)	((Window) mainWindow).selectedSequenceChanged(seq);
	}

	
	/********** Table handling code *************/
	
	/**
	 * This simple class helps entering Double value or null value (empty string) into the table
	 * */	
	public final static class MaybeDouble {
		private Double value;

		public MaybeDouble(String value) {
			if (value.trim().isEmpty())
				this.value = null;
			else
				this.value = new Double(value);
		}

		public Double getValue() {
			return value;
		}
	}

	/**
	 * ParametersTableModel class is a bridge to table data.
	 * */
	public class ParametersTableModel extends AbstractTableModel {
		private static final long serialVersionUID = 1L;
		
		private String[] columns = {"Node", "Attribute", "Model value", "New value"}; 
		private Class<?>[] columnClasses = {String.class, String.class, Double.class, MaybeDouble.class};
		
		/* Following methods return basic table structure */
		
		@Override
		public int getColumnCount() {
			return columns.length;
		}
		
		@Override
		public int getRowCount() {
			if (modifiableParameters != null)
				return modifiableParameters.size();
			return 0;
		}
		
		@Override
		public String getColumnName(int column) {
			return columns[column];
		}
		
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			return columnClasses[columnIndex];
		}

		@Override
		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return columnIndex == 3;
		}

		/**
		 * Returns table cell value
		 * */
		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			if (modifiableParameters != null && scenario != null) {
				ModelInput modelInput = modifiableParameters.get(rowIndex);
				switch (columnIndex) {
				case 0:
					return modelInput.getAcceleratorNode().getId();
				case 1:
					return modelInput.getProperty();
				case 2:
					return modelInput.getDoubleValue();
				case 3:
					ModelInput newModelInput = scenario.getModelInput(
							modelInput.getAcceleratorNode(),
							modelInput.getProperty());
					if (newModelInput != null)
						return newModelInput.getDoubleValue();
					else
						return null;
				}
			}
			return "";
		}
		
		/**
		 * Sets table cell value
		 * */
		@Override
		public void setValueAt(Object value, int rowIndex, int columnIndex) {
			if ((value == null || value instanceof MaybeDouble)
					&& columnIndex == 3 && modifiableParameters != null
					&& scenario != null) {
				Double doubleValue = value == null ? null
						: ((MaybeDouble) value).getValue();
				ModelInput modelInput = modifiableParameters.get(rowIndex);
				if (doubleValue == null)
					scenario.removeModelInput(modelInput.getAcceleratorNode(),
							modelInput.getProperty());
				else
					scenario.setModelInput(modelInput.getAcceleratorNode(),
							modelInput.getProperty(), doubleValue);
			}
		}		
	}
	
	public ParametersTableModel getParametersTableModel() {
		return parametersTableModel;
	}

	
	/********** Running simulations *************/
	
	
	/** Runs the scenario and collects graph data */
	public void run() {
		twissX.removeAllPoints();
		twissY.removeAllPoints();
		twissZ.removeAllPoints();
		energy.removeAllPoints();
		
		try {
			if (scenario == null)
				return;

			scenario.resetProbe();
			scenario.resync();

			// Running simulation
			scenario.run();

			// Getting results
			Trajectory trajectory = scenario.getProbe().getTrajectory();
			Iterator<ProbeState> iterState = trajectory.stateIterator();
						
			while (iterState.hasNext()) {
				EnvelopeProbeState ps = (EnvelopeProbeState) iterState.next();

				double s = ps.getPosition();
				Twiss[] twiss = ps.twissParameters();
				twissX.addPoint(s, twiss[0].getBeta());
				twissY.addPoint(s, twiss[1].getBeta());
				twissZ.addPoint(s, twiss[2].getBeta());
				energy.addPoint(s, ps.getKineticEnergy());				
			}			
		} catch (ModelException e) {
			FrameApplication.displayError("Simulation error",
					"Error while running simulation:", e);
		}
	}

	public BasicGraphData getTwissX() {
		return twissX;
	}
	
	public BasicGraphData getTwissY() {
		return twissY;
	}
	
	public BasicGraphData getTwissZ() {
		return twissZ;
	}
	
	public BasicGraphData getEnergy() {
		return energy;
	}

	/**
	 * Adds actions to the menus
	 */
	@Override
	protected void customizeCommands(Commander commander) {
		// run simulation
		AbstractAction runSim = new AbstractAction("run-sim") {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				run();
			}
		};
		commander.registerAction(runSim);
		
		// load a probe
		AbstractAction loadProbe = new AbstractAction("load-probe") {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				if (probeFileChooser.showOpenDialog(getAcceleratorWindow()) == JFileChooser.APPROVE_OPTION) {
					try {
						Probe probe = ProbeXmlParser.parse(probeFileChooser
								.getSelectedFile().toURI().toURL().toString());
						if (probe instanceof EnvelopeProbe) {
							Document.this.probe = probe;
							if (scenario != null)
								scenario.setProbe(probe);
						} else
							FrameApplication
									.displayError("Invalid probe",
											"Application only works with envelope probes.");

					} catch (ParsingException | MalformedURLException e1) {
						FrameApplication.displayError("Bad probe",
								"Could not load probe:", e1);
					}
				}
			}
		};
		commander.registerAction(loadProbe);
	}
	

	/********** Loading and saving *************/
	
	/** 
	 * We separated XML serializer into an inner class instead of implementing it on main class.
	 * Benefit of this is we can have more serializer for instance for different document version.
	 * */
	private DataListener serializer100 = new DataListener() {
		@Override
		public String dataLabel() {
			return "simeditor";
		}

		/** Writes the XML */
		@Override
		public void write(DataAdaptor adaptor) {
			adaptor.setValue("version", "1.0.0");
			adaptor.setValue("date", new java.util.Date().toString());
			
			// accelerator applications have to take care of saving accelerator path and section themselves		
			if (getAccelerator() != null) {
				adaptor.setValue("acceleratorPath", getAcceleratorFilePath());
				AcceleratorSeq sequence = getSelectedSequence();
				if (sequence != null) {
					adaptor.setValue("sequence", sequence.getId());
				}
			}

			// save the probe
			DataAdaptor probeAdaptor = adaptor.createChild("probe");
			// Before saving the probe needs to be reset
			// otherwise we might be saving it together with final state
			probe.reset(); 
			probe.save(probeAdaptor);
			
			// save the modified values
			DataAdaptor valuesAdaptor = adaptor.createChild("values");
			for (ModelInput modelInput : modifiableParameters) {
				ModelInput newModelInput = scenario.getModelInput(
						modelInput.getAcceleratorNode(),
						modelInput.getProperty());
				if (newModelInput != null) {
					DataAdaptor node = valuesAdaptor.createChild("value");
					node.setValue("node", modelInput.getAcceleratorNode()
							.getId());
					node.setValue("property", modelInput.getProperty());
					node.setValue("newValue", newModelInput.getDoubleValue());
				}
			}
		}

		
		/** Code that read the XML file */
		@Override
		public void update(DataAdaptor adaptor) {
			// First we load the accelerator - AcceleratorApplication has us handle this ourselves
			// But this is great, since we already have all structures when loading the rest of document.
			if (adaptor.hasAttribute("acceleratorPath")) {
				String acceleratorPath = adaptor.stringValue("acceleratorPath");
				Accelerator accelerator = applySelectedAcceleratorWithDefaultPath(acceleratorPath);

				if (accelerator != null && adaptor.hasAttribute("sequence")) {
					String sequenceID = adaptor.stringValue("sequence");
					// here also selectedSequenceChanged() is already called
					setSelectedSequence(getAccelerator().findSequence(sequenceID));					
				}
			}

			// loads the probe
			try {
				probe = Probe.readFrom(adaptor.childAdaptor("probe"));
			} catch (ParsingException e) {
				e.printStackTrace();
			}
			if (scenario != null) scenario.setProbe(probe);

			// loads the modified values
			for (DataAdaptor node : adaptor.childAdaptor("values").childAdaptors("value")) {
				String nodeId = node.stringValue("node");
				String property = node.stringValue("property");
				double newValue = node.doubleValue("newValue");
				AcceleratorNode accNode = getAccelerator().getNode(nodeId);
				scenario.setModelInput(accNode, property, newValue);
			}
		}
	};

	/**
	 * This method saves the content of this document.
	 * 
	 * @see xal.application.XalAbstractDocument#saveDocumentAs(java.net.URL)
	 * 
	 * @param url Path to the file to which to save the document.
	 */
	@Override
	public void saveDocumentAs(URL url) {
		writeDataTo(serializer100, url);
	}

	/**
	 * This method is provided as a helper in the template to load the content.
	 * 
	 * @param url path to the file to load
	 * */
	public static Document load(URL url) {
		Document document = new Document();
		final DataAdaptor documentAdaptor = XmlDataAdaptor.adaptorForUrl(url,
				false);
		document.serializer100.update(documentAdaptor
				.childAdaptor(document.serializer100.dataLabel()));
		return document;
	}	
}
