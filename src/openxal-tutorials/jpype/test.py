
from jpype import *
import os

startJVM(getDefaultJVMPath(), '-ea', "-Djava.class.path=%s" % os.environ['CLASSPATH'])

SEQUENCE = "mebt"
xal = JPackage('xal')
accel = xal.smf.data.XMLDataManager.loadDefaultAccelerator()
seq = accel.getSequence(SEQUENCE)
for i in range(0, seq.getNodeCount()):
    print seq.getNodes()[i]

shutdownJVM()

