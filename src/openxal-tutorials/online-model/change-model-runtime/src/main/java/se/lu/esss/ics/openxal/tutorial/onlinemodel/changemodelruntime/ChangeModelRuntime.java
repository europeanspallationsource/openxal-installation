package se.lu.esss.ics.openxal.tutorial.onlinemodel.changemodelruntime;

/**
 * @author Marc Munoz
 *
 */

import java.awt.Color;
import java.util.Iterator;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import xal.extension.widgets.plot.BasicGraphData;
import xal.extension.widgets.plot.FunctionGraphsJPanel;
import xal.model.ModelException;
import xal.model.alg.EnvelopeTracker;
import xal.model.probe.Probe;
import xal.model.probe.traj.EnvelopeProbeState;
import xal.model.probe.traj.ProbeState;
import xal.model.probe.traj.Trajectory;
import xal.sim.scenario.AlgorithmFactory;
import xal.sim.scenario.ProbeFactory;
import xal.sim.scenario.Scenario;
import xal.smf.Accelerator;
import xal.smf.AcceleratorNode;
import xal.smf.AcceleratorSeq;
import xal.smf.data.XMLDataManager;
import xal.smf.proxy.ElectromagnetPropertyAccessor;
import xal.tools.beam.Twiss;


public class ChangeModelRuntime {

	/**
	 * @param args
	 * @return
	 */

	public class Optics {
		public Optics(int n)
		{
			aSize = n;
		    s = new double[aSize];
			bx = new double[aSize];
			by = new double[aSize];
			bz = new double[aSize];
		}

		public double [] s;
		public double [] bx;
		public double [] by;
		public double [] bz;
		public int aSize;
	}

	private Optics GetOptics(Trajectory trajectory) {
		
		Iterator<ProbeState> iterState = trajectory.stateIterator();
		int i = 0;
		Optics theOptics = new Optics(trajectory.numStates());
		Twiss[] twiss;
		while (iterState.hasNext()) {
			EnvelopeProbeState ps = (EnvelopeProbeState) iterState.next();
			theOptics.s[i] = ps.getPosition();
			twiss = ps.twissParameters(); // gettwiss is deprecated.
			theOptics.bx[i] = twiss[0].getBeta();
			theOptics.by[i] = twiss[1].getBeta();
			theOptics.bz[i] = twiss[2].getBeta();
			i = i + 1;
		}

		return theOptics;

	};
	private void PlotOptics(Optics theOptics) {
		BasicGraphData myDataX = new BasicGraphData();	
		BasicGraphData myDataY = new BasicGraphData();
		myDataX.addPoint(theOptics.s, theOptics.bx);
		myDataY.addPoint(theOptics.s, theOptics.by);
		JFrame theFrame = new JFrame();
		FunctionGraphsJPanel plot = new FunctionGraphsJPanel();
		plot.addGraphData(myDataX);
		plot.addGraphData(myDataY);
		plot.setGraphColor(0, Color.RED);
		plot.setGraphColor(1, Color.BLUE);
		plot.refreshGraphJPanel();
		theFrame.setSize(500,500);
		theFrame.add(plot);
		theFrame.setVisible(true);
		theFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE); 
	}

	public ChangeModelRuntime() throws InstantiationException, ModelException {
		System.out.println("Running"); 
		Accelerator accelerator = XMLDataManager.acceleratorWithPath("../config/main.xal");
		
		if (accelerator == null) {
			System.out
			.println("Accelerator is empty. Could not load the default acceelrator;");
			return;
		}
		AcceleratorSeq sequence = accelerator.findSequence("MEBT");
		EnvelopeTracker envelopetracker  = AlgorithmFactory.createEnvelopeTracker(sequence);
		Probe probe = ProbeFactory.getEnvelopeProbe(sequence,envelopetracker);
		Scenario scenario = Scenario.newScenarioFor(sequence);
		scenario.setSynchronizationMode(Scenario.SYNC_MODE_DESIGN);
		scenario.setProbe(probe);
		scenario.resync();
		scenario.run();
		System.out.println("Scenario run");
		Trajectory trajectory = probe.getTrajectory();
		Optics theOptics = GetOptics(trajectory);
		PlotOptics(theOptics);
		//
		// Making changes to one quad
		//
		AcceleratorNode mag = scenario.nodeWithId("MEBT_Mag:QH01");
		Map<String, Double> magValues = scenario.propertiesForNode(mag);
   	    scenario.setModelInput(mag, ElectromagnetPropertyAccessor.PROPERTY_FIELD, -15);
   	    probe.reset();
		scenario.resync();
		scenario.run();
		Map<String, Double> newmagValues = scenario.propertiesForNode(mag);
		System.out.println("Change value of MEBT_Mag:QH01 from "+magValues+" to "+newmagValues);
		Trajectory trajectory2 = probe.getTrajectory();
		Optics theOptics2 = GetOptics(trajectory2);
		PlotOptics(theOptics2);

		
		System.out.println("End");

	}

	public static void main(String[] args) throws InstantiationException, ModelException {
		// TODO Auto-generated method stub
		ChangeModelRuntime therun = new ChangeModelRuntime();
	}

}

