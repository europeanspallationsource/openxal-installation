#
# OLM Step 6
# 
# Author: Christopher K. Allen <allenck@ornl.gov>
# Modified for ESS Optics by: Miroslav Pavleski <miroslav.pavleski@cosylab.com>
#
# Running the model and retrieving the results.
# We create the SMF hardware objects,
# we instantiate objects necessary for the Online Model, and the
# online model itself.  The online model is run and we retrieve some
# simulation data which is printed to standard output.
#

#import the XAL hardware objects (from SMF)
from xal.smf import Accelerator
from xal.smf import AcceleratorSeq
from xal.smf import AcceleratorNode
from xal.smf.data import XMLDataManager

from xal.smf.proxy import ElectromagnetPropertyAccessor


# Import Online Model objects
from xal.model.probe import EnvelopeProbe
from xal.model.alg import EnvTrackerAdapt

# Import simulation tools
from xal.sim.scenario import AlgorithmFactory
from xal.sim.scenario import ProbeFactory

# Import the XAL online model
from xal.sim.scenario import Scenario

#
#    Global Constants
#

# The target sequence identifier
STR_SEQ_ID      = "mebt"

# Stop and Start Node IDs
STR_ID_START = "QP1"    # simulation start location
STR_ID_END   = "QP4"    # simulation end location

#
# Global Objects
#

# read the accelerator and retrieve the target sequence
GBL_ACCEL = XMLDataManager.loadDefaultAccelerator()
GBL_SEQ   = GBL_ACCEL.getSequence(STR_SEQ_ID)

# Get the hardware object with the given ID and its position
NODE_START    = GBL_SEQ.getNodeWithId(STR_ID_START)
DBL_POS_START = GBL_SEQ.getPosition(NODE_START)


###################################
#
#   Script Start
#
###################################

# create and initialize an algorithm
etracker = AlgorithmFactory.createEnvTrackerAdapt(GBL_SEQ)

# set some custom parameters
etracker.setMaxIterations(10000)
etracker.setAccuracyOrder(1)
etracker.setErrorTolerance(0.001)


# create and initialize a probe
probe = ProbeFactory.getEnvelopeProbe(GBL_SEQ, etracker)
probe.setBeamCurrent(0.038);
probe.setKineticEnergy(885.e6);
probe.setSpeciesCharge(-1);
probe.setSpeciesRestEnergy(939.29e6)

probe.initialize()

# Create and initialize the model to the target sequel
model = Scenario.newScenarioFor(GBL_SEQ);

# Set the probe to simulate, the synchronization model, and the starting node
model.setProbe(probe)
model.setSynchronizationMode(Scenario.SYNC_MODE_DESIGN)
model.resync()
model.setStartNode(STR_ID_START)

#
# Run the Model
#

model.run()


# Retrieve the probe from the model then the trajectory object from the probe
probe = model.getProbe()        # this isn't necessary since we already have it
traj = probe.getTrajectory()

# Retrieve the final state of the simulation
dataFinal = traj.finalState()

# If the probe is an EnvelopeProbe, we can get the covariance matrix of the final state
matCovFin =  dataFinal.getCovarianceMatrix()

# We can get all the covariance matrices of the trajectory
lstCovMat = []
for state in traj:
    matCov = state.getCovarianceMatrix()
    lstCovMat.append( matCov )

# We can print out all the envelope sizes
print "Envelope  sigX      sigY      sigZ"
for matCov in lstCovMat:
    print matCov.getSigmaX(), matCov.getSigmaY(), matCov.getSigmaZ()

