#!/bin/bash
#!-*-sh-*-
#
# Abs: meme-optics-server-localdb.sh executes the MEME optics server. The optics server is
#      an EPICS V4 service, named "optics", which gets model data out of a database.
#
# Rem: The optics service gets accelerator mathematical model data out of a
#      database such as Oracle or PostgresQL, and can compute 
#      inter-element response matrices and process other arguments
#
# Usage:
#      Example, starting in local mode (data will be fake in local mode). 
#      bash
#      (from meme/services/optics)
#      ./bin/meme-optics-server-localdb.sh
#
# Tests: 
#      eget -s optics -a q=QUAD:LTU1:880:twiss -a run=45976
#
# Ref: 
# ----------------------------------------------------------------------------
# Auth: 03-Sep-2014, Greg White (greg@slac.stanford.edu)
# Mod:  14-Apr-2015, Adapted for ESS.
#
# ============================================================================

. $(dirname "${BASH_SOURCE[0]}")/../openxal-environment.sh

function usage {
echo "
  Usage: 
       meme-optics-localdb.sh [-{p,d,l}] [-n {fake, real}]
 
       where:
           -p  - Server is intended for production. 
                   This script should be being executed for production 

           -d   - Server is intended for software development and testing.  
                   The server may contact a different data asources,
                   or otherwise configured differently.

           -l   - Server is intended for initial software development 
                  and testing, on a localhost, like laptop. No connection 
                   is made to the database and data mode is set to fake data.

            ** Disambiguation of pvs for -p or -d is done by client 
               side setting of EPICS_PVA_ADDR_LIST, since a development
               ssytem will run on a development host, while production
               runs on a production host, on the production network **

           The default is -d, start a development server. 

          -n The data mode; if given must be one of REAL or FAKE

       if -l = local, then -n = fake is obligatory.

     Examples
       ./meme-optics-localdb.sh -p
       ./meme-optics-localdb.sh -d -n fake


"
}

# Process server execution environment args. 
#
emode=prod
dmode=real
while getopts pdln: opt
do
    case "$opt" in
       p) emode=prod;;
       d) emode=dev;;
       l) emode=local;dbpwd=${OPTARG};;
       n) dmode=${OPTARG};;
       h) usage; exit 0;;
       *) echo "Unknown option"; exit 1;;
    esac
done
shift $((OPTIND-1))
if [ $# -gt 0 ]; then
	echo "Unknown argument $1"
	exit 0
fi

# Define the name of the MEME server, used for logging, process
# identification etc.
#
export SERVER_NAME=meme_optics
export INSTANCE_NAME=${SERVER_NAME}_${emode}

# Postgress 
#
RDB_CONNECTION_URI=jdbc:postgresql://localhost:5432/machine_model

# "MODE" specfic options. Mode refers to whether this server instance is run
# for development or for production.
#
MODE=`(echo $emode|tr '[[:lower:]]' '[[:upper:]]')`
PORTOPT=""
if [ x${MODE} == x"LOCAL" ]; then
    # options for running on a local host 
   echo Setting up local execution mode
   dmode="FAKE"
elif [ x$MODE == x"DEV" ]; then
   # options for running in development mode 
   echo Setting up development/test execution mode
fi
DATA_MODE=`(echo $dmode|tr '[[:lower:]]' '[[:upper:]]')`

# Logging
#
# Write header environment under which server is being started.
#
function header()
{
    echo "******************************************************************"
    echo Log file: ${log_file}
    echo "Starting ${SERVER_NAME} at `date +'%FT-%H-%M-%S'`"
    echo "from ${PWD} as $USER on"
    echo "`uname -a`"
    echo "******************************************************************" 
    echo
    echo 'WHOLE ENVIRONEMNT (see beneath for summary environment)'
    env | sort
    echo ----------------------------------------------------
    echo SUMMARY ENVIRONMENT
    echo 
    echo CLASSPATH = $CLASSPATH
    env | grep EPICS_
    echo ----------------------------------------------------
}  
LOGGINGROOT=$OPENXAL_HOME/tmp/meme/${INSTANCE_NAME}
mkdir -p $LOGGINGROOT
log_file=${LOGGINGROOT}/`date +"%FT-%H-%M-%S"`.log
header 2>&1 | tee $log_file

# Kill any existing instance of the same mode
pkill -f "${INSTANCE_NAME}"  2>&1 | tee -a $log_file

# Start the server 
#

exec -a ${INSTANCE_NAME} java -server  ${PORTOPT} \
  -DMEME_MODE=${MODE} \
  -DDATA_MODE=${DATA_MODE} \
  -DSERVER_NAME=${SERVER_NAME} \
  -DINSTANCE_NAME=${INSTANCE_NAME} \
  -DCONNECTION_URI_PROPERTY=${RDB_CONNECTION_URI} \
  -DCONNECTION_USERID_PROPERTY=machinemodel \
  -DCONNECTION_PWD=physicsl1nacstuff \
  -Djava.util.logging.config.file=${OPENXAL_CONFIG_DIR}/meme-logging.properties \
  -jar "$OPENXAL_HOME/lib/openxal/openxal.apps.meme.optics-$OPENXAL_VERSION.jar" 2>&1 | tee -a $log_file &

