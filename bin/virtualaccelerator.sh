#!/bin/bash 
. $(dirname "${BASH_SOURCE[0]}")/../openxal-environment.sh
java -jar "$OPENXAL_HOME/lib/openxal/openxal.apps.virtualaccelerator-$OPENXAL_VERSION.jar" -Dcom.cosylab.epics.caj.CAJContext.addr_list=127.0.0.1 -Dcom.cosylab.epics.caj.CAJContext.auto_addr_list=false
