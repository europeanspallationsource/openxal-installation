#!/bin/bash
#!-*- sh -*-
#
# Abs: rdbServerRunner executes the MEME "rdb" service. The rdb service
#      is a general purpose EPICS V4 rpc style service for 
#      efficiently getting data out of a relational database and returning
#      is to an EPICS PVA client (such as an IOC or user). 
#
# Use Case: Execute rdbServerRunner to start the rdbServer prior to executing 
#      pvget commands to get data by pv from Oracle.
#      See usage function below
# 
# Usage example: 
#      bash
#      export MEMEROOT=/home/gregorywhite/Development/meme
#      ./services/rdb/bin/rdbServerRunner -p
#
# Tests: 
#      eget -s rdb -a q=LCLS/BSA.PVs.byZ
#      eget -s names -a pattern=BPMS:IN20:%:Z
# 
# Ref: 
# ----------------------------------------------------------------------------
# Auth: 13-Oct-2011, Greg White (greg@slac.stanford.edu) while at PSI
# Mod:  10-May-2014, Greg White (greg@slac.stanford.edu)
#       Upgraded to passwordless authentication
#       19-May-2014, Greg White (greg@slac.stanford.edu)
#       Added starting under prod, dev, sci, local mode.
#       10..20-Nov-2013, Greg White (greg@slac.stanford.edu)
#       Major development
# ============================================================================

. $(dirname "${BASH_SOURCE[0]}")/../openxal-environment.sh

function usage {
echo "
  Usage: 
       meme-rdb-server-localdb.sh [-{p,d,l}] [-h]
 
     eg
       meme-rdb-server-localdb.sh -p     - Starts the production instance

       where:
           -p  - Server is intended for production. 
                   This script should be being executed on DMZ production network 
                   host, and the executable will be set to broadcast 
           -d   - Server is intended for software development and testing;
                   and executable will be set to broadcast (ie exactly as production).
                   The server may take different actions, such as contacting
                   development data sources.
           -l   - Server is intended for initial software development and testing, 
                   on a localhost, like laptop.

            ** Disambiguation of pvs for -p or -d is done by client 
               side setting of EPICS_PVA_ADDR_LIST, since a development
               ssytem will run on a development host, while production
               runs on a production host, on the production network **

           The default is -d, start a development server. 

        -h Prints this help.
"
}

# Process server execution environment args. 
#
mode=prod
while getopts pdl opt
do
    case "$opt" in
       p) mode=prod;;
       d) mode=dev;;
       l) mode=local;;
       h) usage; exit 0;;
       *) echo "Unknown option"; exit 1;;
    esac
done
shift $((OPTIND-1))
if [ $# -gt 0 ]; then
	echo "Unknown argument $1"
	exit 0
fi

# Define the name of the MEME server, used for logging, process identification etc.
#
export SERVER_NAME=meme_rdb
export INSTANCE_NAME=${SERVER_NAME}_${mode}

#
# Locations
#

# Postgress 
RDB_CONNECTION_URI=jdbc:postgresql://localhost:5432/machine_model

# EPICS PVA Config.
# SLAC overrides default EPICS_PVA_BROADCAST_PORT, but ESS doesn't.
#export EPICS_PVA_BROADCAST_PORT=5056
#export EPICS_PVA_SERVER_PORT=5055

# Set execution options depending on mode.
#
# If the mode is "science", explicitly set the port number, which must be
# known by a client too.
MODE=`(echo $mode|tr '[[:lower:]]' '[[:upper:]]')`
PORTOPT=""
if [ x$MODE == x"LOCAL" ]; then
   # options for running on a local host
   echo Setting up local execution mode
elif [ x$MODE == x"DEV" ]; then
   # options for running in development mode
   echo Setting up development mode
fi

# Logging
#
# Write header environment under which server is being started.
function header()
{
    echo "*************************************************************************"
    echo Log file: ${log_file}
    echo "Starting ${SERVER_NAME} at `date +'%FT-%H-%M-%S'`"
    echo "from ${PWD} as ${USER} on "
    echo "`uname -a`"
    echo "*************************************************************************" 
    echo
    echo 'WHOLE ENVIRONEMNT (see beneath for summary environment)'
    env | sort
    echo ----------------------------------------------------
    echo SUMMARY ENVIRONMENT
    echo 
    echo CLASSPATH = $CLASSPATH
    echo LD_LIBRARY_PATH = $LD_LIBRARY_PATH
    echo TNS_ADMIN = $TNS_ADMIN 
    echo ----------------------------------------------------
}  
LOGGINGROOT=${OPENXAL_HOME}/tmp/meme/${INSTANCE_NAME}
mkdir -p $LOGGINGROOT
log_file=${LOGGINGROOT}/`date +"%FT-%H-%M-%S"`.log
header 2>&1 | tee $log_file

# Kill any existing instance of the same mode
pkill -f "${INSTANCE_NAME}"  2>&1 | tee -a $log_file

# Start the service as a pvaccess RPC server.  
# Use exec -a to make the process name rdbservice (for ps, pgrep etc)
# 
exec -a ${INSTANCE_NAME} java -server ${PORTOPT} \
  -DMEME_MODE=${MODE} \
  -DSERVER_NAME=${SERVER_NAME} \
  -DINSTANCE_NAME=${INSTANCE_NAME} \
  -DCONNECTION_URI_PROPERTY=${RDB_CONNECTION_URI} \
  -DCONNECTION_USERID_PROPERTY=machinemodel \
  -DCONNECTION_PWD=physicsl1nacstuff \
  -DRPCQUERIES_FILENAME=${OPENXAL_CONFIG_DIR}/meme-rdb.xdb \
  -Djava.util.logging.config.file=${OPENXAL_CONFIG_DIR}/meme-logging.properties \
  -jar "$OPENXAL_HOME/lib/openxal/openxal.apps.meme.rdb-$OPENXAL_VERSION.jar" 2>&1 | tee -a $log_file &
