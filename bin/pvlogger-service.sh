#!/bin/bash 
match=0 
SERVICE_IDS=("PHYSICS" "DEFAULT" "ES&H" "TESTFAC")

picked_id=${1:-"PHYSICS"}
for id in "${SERVICE_IDS[@]}"; do
	if [[ "$id" = "$picked_id" ]]; then
		match=1
		break
	fi
done

if [[ $match = 0 ]]; then
	echo "Invalid service id! (options: ${SERVICE_IDS[*]})"
	exit 1
fi

. $(dirname "${BASH_SOURCE[0]}")/../openxal-environment.sh
java -DserviceID="$picked_id" -jar "$OPENXAL_HOME/lib/openxal/openxal.services.pvlogger-$OPENXAL_VERSION.jar"
