#!/bin/bash
address=physics02.esss.lu.se
port=5064
repeaterPort=5065
connectionTimeout=30
beaconPeriod=15
maxArrayBytes=16348

echo "Script starting."
echo "Setting JAVA_TOOL_OPTIONS to add system property"
export JAVA_TOOL_OPTIONS=$JAVA_TOOL_OPTIONS" -Djca.use_env=true"
echo "Setting EPICS_CA_ADDR_LIST to $address."
export EPICS_CA_ADDR_LIST=$address
echo "Setting EPICS_CA_SERVER_PORT to $port."
export EPICS_CA_SERVER_PORT=$port
echo "Setting EPICS_CA_REPEATER_PORT to $repeaterPort."
export EPICS_CA_REPEATER_PORT=$repeaterPort
echo "Setting EPICS_CA_CONN_TMO to $connectionTimeout."
export EPICS_CA_CONN_TMO=$connectionTimeout
echo "Setting EPICS_CA_BEACON_PERIOD to $beaconPeriod."
export EPICS_CA_BEACON_PERIOD=$beaconPeriod
echo "Setting EPICS_CA_MAX_ARRAY_BYTES to $maxArrayBytes."
export EPICS_CA_MAX_ARRAY_BYTES=$maxArrayBytes


echo "Script finished. You may now run your programs."
echo "NOTE: Programs that use Virtual accelerator must be run in the same shell as this script."
