#!/bin/bash 
. $(dirname "${BASH_SOURCE[0]}")/../openxal-environment.sh
llurl=$1
shift
if [ -z "$llurl" ]; then
  llurl=https://f6dea02762c7d72c87ecd77e9ac9c92f0cd2fa43.googledrive.com/host/0B_mauLIA30CDVzY4NWdXODBNU3c/LinacLego/master/xml/linacLego.xml
fi
java -cp "$OPENXAL_HOME/lib/openxal/openxal.library-$OPENXAL_VERSION.jar" se.lu.esss.linaclego.LinacLego "$llurl" "$@"

