#!/bin/bash 
. $(dirname "${BASH_SOURCE[0]}")/../openxal-environment.sh
cd $OPENXAL_HOME
java -jar "$OPENXAL_HOME/lib/openxal/openxal.apps.launcher-$OPENXAL_VERSION.jar" $@
