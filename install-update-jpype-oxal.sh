#!/bin/bash
# Git upgrades JPype and installs it to user home
#
# Author: ivo.list@cosylab.com
#

# Execute everything in the base dir of the script

cd "`dirname "${BASH_SOURCE[0]}"`" > /dev/null
. ./openxal-environment.sh

# only add user and prefix option if not run by root
if [ "$(id -u)" != "0" ]; then
   SOPT="--user --prefix="
fi

# 1. Configure OpenXAL Python bridge
cd jpype_config
python setup.py install $SOPT

