#!/bin/bash
#
# Installation script:
#  - install ess-java-config maven package
#  - install .JCAProperties file if not already installed
#  - make configuration files using absolute paths
#
# Author: ivo.list@cosylab.com, blaz.kranjc@cosylab.com
#

# 1. Install newest ess-java-config package using maven

echo "Installing newest ESS Java Config"
./apache-maven-3.2.3/bin/mvn install -f src/ess-java-config/pom.xml

# 2. Download jars from artifactory
echo "Downloading OpenXAL jars."
./lib/openxal/upgrade-openxal

# 3. Install .JCAProperties

if [ -f ~/.JCALibrary/JCALibrary.properties ]; then
  echo "JCA library properties already installed (not changing)"
else
  echo "Installing JCA library properties"
  mkdir -p ~/.JCALibrary
  cp conf/JCALibrary.properties ~/.JCALibrary/JCALibrary.properties
fi

# 4. Make configuration files using absolute paths

conf/make-config-files.sh

