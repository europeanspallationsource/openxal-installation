#!/bin/bash
# Create and set up database for use by openxal-gui
# Should be run  as sudo {PATH}/openxal-gui-db-installation.sh
# Author: blaz.kranjc@cosylab.com,
#

. $(dirname "${BASH_SOURCE[0]}")/../openxal-environment.sh

export DATABASE=pvlog #name of the database
export USER=pvlog #name of the user
export PASSWORD=pvlog #password for the user
export SCHEMA="pvlog" # name of the schema
export CONFIG_FILE=$OPENXAL_HOME/conf/create-pvlogger-postgres.sql #database configuration file (requies defauld schema and database name)
export OPTIONS="--quiet" #psql invocation options"
export PSQL=psql

$OPENXAL_HOME/conf/initialize-db.sh
