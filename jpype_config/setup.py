#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup,find_packages

import os
import sys

# Path to installation:
OXAL_HOME=os.environ['OPENXAL_HOME']
JAVA_HOME=os.environ['JAVA_HOME']

OXAL_VERSION=os.environ['OPENXAL_VERSION']

# The OpenXAL jar file:
OXAL_LIBRARY=os.environ['OPENXAL_LIBRARY']

text=open('oxal/__init__.py.macro','rt').read()
formatted=text.format(**locals())
open('oxal/__init__.py','w').write(formatted)

setup(
        name='OpenXAL',
        version=OXAL_VERSION,
        description='Python config for OpenXAL',
        url='http://xaldev.sourceforge.net/',
        install_requires=['JPype1>=0.6.1'],
        packages=find_packages(),
)
